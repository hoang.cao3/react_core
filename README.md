# React_core

Project web admin with reactJS + Typescript

I. How to run project?

- npm install (Pull package)

1. Environment dev
   - npm run dev
2. Environment staging
   - npm run staging
3. Environment production
   - npm run production
4. Run test
   - npm run lint
   - npm run lint:fix

II. Config

- Variable environment: root/evn
- Webpack config: root/config
- Theme config: root/theme.ts

II Package Recommend

- tailwindcss
- react-use
- antdesign
